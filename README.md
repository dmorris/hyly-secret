# Usage
If you think that this code could help you, please submit an issue.

This is published as a portfolio piece.

This project was designed to output the files `secret-plots-kerf-play.dxf` (for slide rule faces) and `veiled-intentions` (for the slide rule middle layer) to be imported into RDWorks and sent to a laser cutter. This was done by invoking `secret-plots.hy` and `veiled-intentions.hy` from a command line.

This project requires hylang and ezdxf.

# Files

- `draughty.hy` is the basic set of transformations and primitive shapes for implementing the domain-specific language for representing a 2D scene graph homeomorphically used in this project.

- `secret-plots.hy` and `veiled-intentions.hy` are entry points and contain the top-level code describing the ring.

- `secret_machinations.hy` does the work of describing how to make various parts of the ring. It uses elements from `draughty.hy` to define different sized number marks, bands, and full scales of a slide rule.
