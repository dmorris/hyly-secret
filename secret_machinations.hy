#!/usr/bin/env hy
;;; The ring.  Useful functions.
(import [draughty [*]])
(import [math [log]])
(require [draughty [*]])


;; define a pair of bands.  They shouldn't need to be counter-sunk, but we will see.
;; Uwe's advice: 1% clearance. We'll try the beam at first.
;; Should we trace it twice or not?
(defn bands [inner outer split &optional clearance]
  "Draw a pair of bands."
  (circle inner)
  (circle outer)
  (setv middle
        (+ (* split outer)
           (* (- 1 split) inner)))
  (if clearance
      (do
        (setv clearance (/ clearance 2))
        (circle (+ middle clearance))
        (circle (- middle clearance)))
      (circle middle)))

(defn big-mark [number]
  "make a mark for an addition scale or a primary multiplication mark"
  (setv xoffsets {0 -0.5
                  1 0
                  2 -0.5
                  3 -0.5
                  4 -0.6})
  (color 2 (rot 90 (line 8)))
  (tr (, (- (get xoffsets number) 1.5) 7)
      (scale 7 (txt (str number))))
  (tr (, (- (get xoffsets number) 1.5) -14.5)
      (scale 7 (txt (str number)))))

;; Minor mark style
(defn lil-mark [number]
  (color 2
         (rot 90 (line 5))
         (tr (, 0 2.5) (line 1))
         (tr (, 0 -2.5) (line 1)))
  (tr (, -1 5)
      (scale 4 (txt (str number))))
  (tr (, -1 -7.5)
      (scale 4 (txt (str number)))))

;; Addition Scale
(defn add-scale [mod inner outer split &optional clearance]
  (color 3 (bands inner outer split clearance))
  (for [n (range mod)]
    (rot (* -360.0 (/ n mod))
         (tr (, 0 (+ (* split outer)
                     (* (- 1 split) inner)))
             (big-mark n)))))

;; Multiplication Scale
(defn mul-scale [mod inner outer split &optional clearance]
  (for [n (range 1 mod)] ; big marks
    (rot (* -360.0 (log n mod))
         (tr (, 0 (+ (* split outer)
                     (* (- 1 split) inner))) 
             (big-mark n))))
  (for [n (filter (fn [n] (not (or (= 0 (% n mod))
                                   (in n (range mod)))))
                  (range (* mod mod)))]
    (rot (* -360.0 (log n mod))
         (tr (, 0 (+ (* split outer)
                     (* (- 1 split) inner)))
             (lil-mark (% n mod)))))
  (color 3
         (bands inner outer split clearance)))
