;;; Draughty: A drafting environment with lots of holes.
;; Draughty uses ezdxf to produce DXF files with a LAF inspired by OpenSCAD
(import ezdxf [math :as m])
(import sh)
(import pdb)

;; math
(defn rotate [theta coords]
  (setv x (first coords))
  (setv y (second coords))
  (, (+ (* x (m.cos (m.radians theta))) (* -1 y (m.sin (m.radians theta))))
     (+ (* x (m.sin (m.radians theta))) (* y (m.cos (m.radians theta))))))

(defn flip [coords]
  (, (* -1 (first coords)) (* -1 (second coords))))

(defn pp [a b]
  (, (+ (first a) (first b)) (+ (second a) (second b))))


(defn angle-add [a b]
  (-> a (+ b) (% 360)))

;;; set up state so we can ignore it later
;; file i/o, manager
(setv DRAWING (.new ezdxf :dxfversion "R2010"))
(setv MODELSPACE (.modelspace DRAWING))
(defn saveas [path]
  (global DRAWING)
  (.saveas DRAWING path)
  (setv DRAWING (.new ezdxf :dxfversion "R2010"))
  (setv MODELSPACE (.modelspace DRAWING)))

;; abstract location
(setv LOCATION [(, 0 0)])

(defn curloc []
  (get LOCATION -1))

(defmacro tr [offset &rest body]
  `((fn []
      (global LOCATION)
      ;;(print "Rotating by " (curang) "in tr")
      (.append LOCATION  (pp (curloc)
                             (rotate (curang)
                                     (scale-point ~offset))))
      ((fn [] ~@body))
      (.pop LOCATION))))
;; abstract rotation
(setv ANGLE [0])

(defn curang []
  (global ANGLE)
  (get ANGLE -1))

(defmacro rot [turn &rest body]
  "rotates the contents"
  ;; in macros, we don't push a context.
  ;; we have to push a stack frame or just check if ANGLE is already defined
  ;; it's more correct to push a frame.
  `((fn []
      (global ANGLE)
      (.append ANGLE (angle-add (curang) ~turn))
      ((fn [] ~@body))
      (.pop ANGLE))))

;; abstract color
(setv COLOR [0])

(defn curcolor []
  (get COLOR -1))

(defmacro color [new-color &rest body]
  `((fn []
      (global COLOR)
      (.append COLOR ~new-color)
      ((fn [] ~@body))
      (.pop COLOR))))

;; abstract scaling
(setv SCALE [1])
(defn curscale []
  (get SCALE -1))
(defn scale-point [coords]
  "scale point"
  (, (* (first coords) (curscale)) (* (second coords) (curscale))))

(defmacro scale [factor &rest body]
  `((fn []
      (global SCALE)
      (.append SCALE (* (curscale) ~factor))
      ((fn [] ~@body))
      (.pop SCALE))))
;;; basic things to do
(defn line [length]
  (setv length (* 0.5 length (curscale)))
  (setv half (rotate (curang) (, length 0)))
  (.add-line MODELSPACE
             (pp half (curloc)) (pp (flip half) (curloc))
             :dxfattribs {"color" (curcolor)})) 

(defn circle [radius]
  (setv radius (* (curscale) radius))
  (.add-circle MODELSPACE
               (curloc)
               radius
               :dxfattribs {"color" (curcolor)}))

(setv _cache (dict))
(defn underlay [path &optional (scale 1) (_cache _cache)]
  "load a pdf, dwf, dwfx or dgn as part of the drawing.  You still need the pdf?"
  (if (not (in path _cache))
      (setv (get _cache path)
            (.add_underlay_def DRAWING
              :filename path
              :name 1)))
  (.add_underlay MODELSPACE (get _cache path)
                 :insert (+ (curloc) (, 0))
                 :scale (* scale (curscale))
                 :rotation (curang)))

(defn add-drawing [path]
  "add a drawing to this one"
  (setv otherdwg (ezdxf.readfile path)) ; read another drawing
  (setv othermod (.modelspace otherdwg)); get its modelspace
  (setv otherelms (.query othermod "*")) ; query for all elements in it
  (for [elm otherelms] 
    (print "found an element")
    (.set-dxf-attrib elm "color"                    ; change them all to the current color
                     (curcolor))
    (.unlink_entity othermod elm) ; remove them from their current drawing         
    (.add_entity MODELSPACE elm))) ; add them to the drawing

(defn tex2eps [inpath outpath]
  (sh.pdflatex inpath)
  (sh.convert "-density 3000"
              ))

(defn eps2dxf [inpath outpath]
  (sh.pstoedit "-dt" "-f 'dxf:-polyaslines -mm'"
               inpath outpath))

(defn add-formula [formula]
  (setv temptex (sh.mktemp "--suffix=.tex"))
  (with [f (open temp "r")]
    (.write f "\documentclass{standalone}")
    (.write f "\begin{document}")
    (.write f formula)
    (.write f "\end{document}"))
  (setv tempeps (sh.mktemp "--suffix=.eps"))
  (tex2eps temptex tempeps)
  (setv tempdxf (sh.mktemp "--suffix=.dxf"))
  (eps2dxf tempeps tempdxf)
  (add-drawing tempdxf)
  (map sh.rm [tempdxf tempeps temptex]))

(defn txt [text]
  "add text to the drawing--- this requires rdworks config"
  (.set-pos
    (.add-text MODELSPACE text
               :dxfattribs {"color" (curcolor)
                            "height" (curscale)
                            "rotation" (curang)
                            "style" "CMU Serif"})
    (curloc) :align "MIDDLE_LEFT")) ; :align "MIDDLE_CENTER" RDWorks ignores this.  Cheap chinese crap.
                                ;(setv xoffsets)
(setv xoffsets {"0" -3
                "1" -3
                "2" -3
                "3" 0
                "4" 0
                "5" 0})

;; (defn txt [text] ; can't work because text is nonlinear
;;   (circle (/ (curscale) 4))
;;   (tr (, (get xoffsets text)
;;          (/ (curscale) -100))
;;       (rawtxt text)))

;; BUGS
;; context might get lost inside the macros ---
;; but simple tests don't seem to have this problem
;; figure out how to test that they don't change scope
;; I'll probably have to make the body into a function bound to a gensym
(defn clear []
  (setv DRAWING (.new ezdxf :dxfversion "R2010"))
  (setv MODELSPACE (.modelspace DRAWING)))
