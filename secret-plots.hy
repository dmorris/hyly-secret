#!/usr/bin/env hy
;;; The ring.  Scales.
(import [draughty [*]])
(import [math [log]])
(require [draughty [*]])
(import [secret-machinations [*]])
(require [secret-machinations [*]])


;; test
(mul-scale :mod 5
           :inner 16
           :outer 50
           :split 0.5
           :clearance None)
(tr (, 105 0)
    (add-scale :mod 5
               :inner 16
               :outer 50
               :split 0.5
               :clearance None))
;; (saveas "secret-test-5.dxf")
;; Both scales of a common prime together in bands.

(saveas "secret-plots-kerf-play.dxf")

;; Let's try 5.

;; we might want to put the letters on their own layers, we will see.
