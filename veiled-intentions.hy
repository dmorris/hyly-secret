#!/usr/bin/env hy
;;; The ring.
(import [draughty [*]])
(import [math [log]])
(require [draughty [*]])
(import [secret-machinations [*]])
(require [secret-machinations [*]])

(bands :inner 16
       :outer 50
       :split 0.57
       :clearance 0.05)
(saveas "veiled-intentions.dxf")
